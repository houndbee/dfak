Digital First Aid Kit
============

The Digital First Aid Kit aims to provide preliminary support for people facing the most common types of digital threats. *The Digital First Aid Kit is not meant to serve as the ultimate solution to all your digital emergencies.*

About
=====
The Digital First Aid Kit is a collaborative effort of a number of organizations and individuals in the [Rapid Response Community](https://rarenet.org) and beyond - see our [Contributors list](/CONTRIBUTORS.md).


How to Contribute
============

{Describe how to contribute}


License
==============

Content is licensed as [Creative Commons BY-SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/).
Code is licensed as [GNU General Public License version 3](https://www.gnu.org/copyleft/gpl.html) - {confirm}