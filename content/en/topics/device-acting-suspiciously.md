---
layout: topic
title: My device is acting suspiciously
author: RaReNet
language: en
summary: "If your computer or phone is acting suspiciously, there may be unwanted or malicious software on your device."
date: 2018-09
permalink: /en/topics/device-acting-suspiciously/
parent: /en/
---


# My device is acting suspiciously

Malware attacks have evolved and become highly sophisticated over the years. These attacks pose multiple different threats and can have serious implications to your personal and organisational infrastructure and data.

Malware attacks come in different forms, such as viruses, phishing attacks, ransomware, trojans and rootkits. Some of the threats are: computers crashing, data theft (ie: sensitive account credentials, financial info, bank account logins), an attacker blackmailing you to pay a ransom by taking control of your device or taking control of your devices and use it to conduct DDOS attacks.

Some methods commonly used by attackers to compromise you and your devices seem like regular activities, such as:

- An email or to a link through social media that will tempt you to open an attachment or click on a link.

- Installing software on your device you've download from the internet.

- Entering your username and password into a website that’s made to look legitimate, but isn't.

To prevent yourself from falling prey to an attacker's efforts to compromise your devices and data, always double check the legitimacy of any email you receive, a file you have downloaded or a link asking you for your account login details.

For more information, please follow the tips on ["Avoiding Malware and Phishing Attacks"](https://securityinabox.org/en/guide/malware/#avoiding-malware-and-phishing-attacks) in the Security-in-a-Box guide, [Protect your device from malware and phishing attacks](https://securityinabox.org/en/guide/malware/)

## Workflow

### account_hijacking

Are you sure that your account hasn't been hijacked and/or your password compromised?

- [Yes, it could be account hijacking](#account_hijacking_yes)
- [No](#click_link)

### click_link
Did you open an attachment or clicked on a link?

- [Yes, I clicked on a link or opened an attachment](#clean_up)
- [no](#multiple_compromise)

### multiple_compromise
Has your device acted strangely multiple times, even after you have changed the password?

- [Yes, it keeps acting weird](#all_failed)
- [no](#more_background)

### more_background
Have you experienced this earlier and solved the issue, but want more background info?

- [Go to further support](#further_support)

### clean_up

Anti-virus software can be an effective first response to protecting a device from a significant percentage of malware. However, anti-virus software is generally considered ineffective against targeted attacks, especially by state-sponsored actors. Nevertheless, it remains a valuable defensive tool against non-targeted, but still dangerous, malware. Below is a non-exhaustive list of options:

- [Microsoft Safety Scanner](http://www.microsoft.com/security/scanner/en-us/default.aspx) (Windows)
- [F-Secure](http://www.f-secure.com/en/web/home_global/online-scanner)
- [Kaspersky](http://www.kaspersky.com/security-scan)
- [ClamXav](http://www.clamxav.com/) (Mac OS X)
- [TrendMicro](http://housecall.trendmicro.com/)
- [ClamAV](http://www.clamav.net/lang/en/) (Windows and Linux)
- [ESET](http://www.eset.com/us/online-scanner/) (Windows)

When you run anti-virus software, ensure that it is up to date. If a virus is detected the following steps are recommended.

- Step 1: Ensure that your anti-virus software is up to date
- Step 2: Take a screenshot of the message
- Step 3: Continue with the recommended steps to remove the virus
- Step 4: Following the guidelines in the [Safer Communications section](SecureCommunication.md), send the screenshot to a person with security expertise


### all_failed

- Reinstall your operating system; if you are used to make backups your do not loose anything, if you did not make backups you loose all your data. It is not possible to be sure the virus has been completely removed. After installing one malware, the attacker usually installs others; therefore, it is always recommended to reinstall the operating system after performing a thorough wipe of the hard drive. If possible, investigate whether replacing your hard drive is an option.

- After reinstallation of the operating system you will want to have access to your files again. Be aware that malware could have infected your documents. After reinstalling your operating system, you should take the following steps:

- If possible, retrieve your documents from the back up you made prior to the malware infection.

- If you do not know when your device became compromised with malware, or if you suspect specific attachment and documents to be infected with the malware, there are several things you can do:

- Download all of your executable files again from a trusted source

- If the attack vector has been identified by an technical expert and the malware is clearly infecting other documents, one option could be to upload and open them in Google Docs and re-download them from there. In most cases opening a suspicious document in Google Docs is probably a good recommendation. The document will not infect your computer and it will remain editable.

- Another option is to copy the documents onto a USB key and open them on [CIRCLean](http://circl.lu/projects/CIRCLean/). The malware will not be copied, but the documents will be transformed to an image or pdf, a read only and non-editable format.

When you have chosen to clean your device without understanding the malware and attack first please keep the following in mind:

1. There is no quick fix to clean up malware from you computer. Even after completing the following steps a very sophisticated malware infection may still be present. These steps are sufficient to remove most of the malware you are likely to encounter unless you are being targeted by a very advanced attacker.
2. If you believe that you are being targeted by a state actor and indicators of compromise persist after cleaning up the virus detected through the steps below, disconnect it from the internet, turn off the device, unplug it, if possible remove its battery and [seek the help of a security professional](SecureCommunication.md#seeking-and-providing-remote-help).


### further_support


If your devices have been compromised by a targeted attack, it can be valuable to understand why you've been attacked and by whom.

**Why you've been attacked**: Who do you think might be interested in targeting you or your organization? Is this threat related to your work? In the section on helpful resources there are links to guides that give you tips and tricks on how to prevent digital emergencies and be proactive about your digital security.

**By whom**: What are your adversary's technical capabilities? Is the potential attacker (a government entity or other third party) known to use internet surveillance technology. In the section on Reports on State-sponsored Malware attacks  there is more information on the different ways in which governments have used malware for targeted attacks.

**Documentation**: It will be difficult to remember specifics such as the time and date when you clicked on a suspicious link. Therefore, we recommend keeping a notebook next to your computer to make notes of the time, date and strange things that have happened and are happening to your device. In some cases experts have been able to identify a specific type of malware by correlating the time of the attack with unique characteristics or a possible indicator of compromise.


The following recommendations should only be implemented by a person with some security expertise. If you do not have the necessary expertise to follow the instructions below, [ask a specialist for help](SecureCommunication.md#seeking-and-providing-remote-help). If possible, communicate with them via secure channels using the guidelines in the [Safer Communications section](SecureCommunication.md).


    If you do not have the skills to process this information, pass it on to a trusted, trained malware expert or one of the following organizations:

    - [EFF](https://www.eff.org/) - info@eff.org
    - [Citizen Lab](http://citizenlab.org/) - info@citizenlab.org
    - [CIRCL](http://www.circl.lu/) - info@circl.lu
    - [Amnesty](https://amnesty.org) - tech.emergency@amnesty.org

    if you contact them do think of these points:

- If one of the indicators of compromise is an email, [gather the headers](https://www.circl.lu/pub/tr-07/), and [analyze them](https://support.google.com/mail/answer/29436?hl=en). Google also provides [a simple tool](https://toolbox.googleapps.com/apps/messageheader/) that does this automatically.
- If possible, securely obtain the malware itself and look it up on [Virus Total](https://www.virustotal.com/) with the hashes to see if the file has already been uploaded.
- If the file is not confidential, you can also upload it on [Malwr](https://malwr.com/) and analyze the result.
- If the suspicious file comes from a link, get the full URL and run it in:
    - [URL Query](http://urlquery.net/)
    - [Wepawet](http://wepawet.iseclab.org/)


- Information collection for further analysis
    The following information is critical for any further analysis, by you or by anyone else. It is recommended to collect most - and if possible all - of the information below for further analysis:

    - Information on the system (hardware, OS details, including version and update status)
    - Location of the victim and system localization (source IP, country, language of the user)
    - List of users sharing the same device
    - In case of suspicious email: full headers
    - In case of a link: the full link, timestamp and screenshot
    - It would also be useful to have a dump of the webpage, and a packet capture of the connection to it
        <!-- A tutorial or link might be useful here as well -->
    - [Memory dumps](https://www.circl.lu/pub/tr-22/#memory-acquisition)
    - [Disk images](https://www.circl.lu/pub/tr-22/#disk-acquisition)
        - Consider that it might be easier to recommend replacing a hard-drive rather than teaching people to do a disk image in order to transfer the disk to helpful people.
    - Evaluate possibility of remote forensics and if so, establish proper channel of communication


## Helpful resources

- [Detach from Attachments](https://tibetaction.net/knowledge/tech/attachments/)
- [More on Viruses and Spyware](https://securityinabox.org/en/chapter_1_2)
- Google's Chrome browser and the open source version, Chromium, provide excellent information about suspicious websites
