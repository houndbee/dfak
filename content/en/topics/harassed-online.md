---
layout: page
title: "Are you being targeted by online harassment?"
author: FP
language: en
summary: "Are you being targeted by online harassment?"
date: 2018-09
permalink: /en/topics/harassed-online/
parent: Home
---

# Are you being targeted by online harassment?

Although the Internet, and social media platforms in particular, have become a critical space for civil society members and organizations, especially for women, LGBTI people, and other minorities, to express themselves and make their voices heard, they have also become spaces where these groups are easily targeted for expressing their views. Online violence and abuse denies women, LGBTI persons, and many other unprivileged people the right to express themselves equally, freely, and without fear.

Online violence and abuse has many different forms, and perpetrators can often rely on impunity, also due to a lack of laws that protect victims of harassment in many countries, but most of all because protection strategies need to be tweaked creatively depending on what kind of attack is being launched.

It is therefore important to identify the typology of the attack targeting us to decide what steps we can take.

[Here is a questionnaire](#physical_wellbeing) to plan how to get protected against the attack you are suffering.

## Workflow

### physical_wellbeing

Do you fear for your physical wellbeing?

 - [Yes](Escalation_physical-sec)
 - [No](#no_physical_risk)

### no_physical_risk

Do you think the attacker has accessed or is accessing your device?

 - [Yes](#device_compromised)
 - [No](#account_compromised)

### device_compromised

> Change the password to access your device:
>
> - [Mac OS](https://support.apple.com/en-us/HT202860)
> - [Windows](https://support.microsoft.com/en-us/help/14087/windows-7-change-your-windows-password)
> - [iOS - Apple ID](https://support.apple.com/en-us/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=en)

Has the attacker been effectively locked out of your device?

 - [Yes](#account_compromised)
 - [No](device-acting-suspiciously)

### account_compromised

> If someone got access to your device, they might have also accessed your online accounts,
> so they could be reading your private messages, identify your contacts, and publish
> posts, images, or videos impersonating you.

Have you noticed posts or messages disappearing, or other activities that give you
good reason to think your account may have been compromised?

 - [Yes](account-access-issues)
 - [No](#impersonation)

### impersonation

Is someone impersonating you?

[Yes](impersonation)
[No](#doxing)

### doxing

Has someone published private information or pictures without your consent?

 - [Yes](#doxing_yes)
[No](#hate_speech)

### doxing_yes

Where have your private information or pictures been published?

[On a social networking platform](#doxing_sn)
[On a website](#doxing_web)

### doxing_sn

> If yur private information or pictures have been published in a social media platform,
> you can report a violation of the community standards following the reporting procedures
> provided to users by social networking websites. You will find instructions for
> the main platforms in the following list:
>
> - [Google](https://www.cybercivilrights.org/online-removal/#google)
> - [Facebook](https://www.cybercivilrights.org/online-removal/#facebook)
> - [Twitter](https://www.cybercivilrights.org/online-removal/#twitter)
> - [Tumblr](https://www.cybercivilrights.org/online-removal/#tumblr)
> - [Instagram](https://www.cybercivilrights.org/online-removal/#instagram)

Have the information or media been deleted?

 - [Yes](#one_more_persons)
 - [No](Escalation_harassment)

### doxing_web

> Follow [these instructions](https://withoutmyconsent.org/resources/take-down) to take down content from a website.

Has the content been taken down by the website?

[Yes](#one_more_persons)
[No](Escalation_harassment)

### hate_speech

Is the attack based on attributes like race, gender, or religion?

[Yes](#one_more_persons)
[No](Escalation_harassment)


### one_more_persons

Have you been attacked by one or more persons?

[One person](#one_person)
[More persons](more_persons)

### one_person

Do you know this person?

[Yes](#known_harasser)
[No](#block_harasser)


### known_harasser

> If you know who is harassing you, you can think of reporting them to your country's authorities.
> Each country has different laws for protecting people from online harassment, and
> you should explore the legislation in your country to decide what to do.
>
> If you decide to sue this person, you should reach out to a legal expert.


Do you want to sue the attacker?

 - [Yes](Escalation_legal)
 - [No](#block_harasser)


### block_harasser

> Whether you know who your harasser is or not, it's always a good idea to block
> them on social networking platforms whenever possible.
>
> - [Facebook](https://www.facebook.com/help/290450221052800)
> - [Twitter](https://help.twitter.com/en/using-twitter/blocking-and-unblocking-accounts)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=en)
> - [Tumblr](https://tumblr.zendesk.com/hc/en-us/articles/231877648-Blocking-users)
> - [Instagram](https://help.instagram.com/426700567389543)

Have you blocked your harasser effectively?

 - [Yes](#final-tips)
 - [No](Escalation_harassment)

### more_persons

> If you are being attacked by more than one person, you might be the target of
> a harassment campaign, and you will need to reflect on what is the best strategy
> that applies to your case.
>
> To learn about all the possible strategies, read this [page](https://www.takebackthetech.net/be-safe/hate-speech-strategies)

Have you identified the best strategy for you?

 - [Yes](#final-tips)
 - [No](Escalation_harassment)

### final-tips

- Document
- 2fa

## Helpful Resources

- [What to Do if You're Being Doxed](https://www.wired.com/story/what-do-to-if-you-are-being-doxed/)
- [Locking Down Your Digital Identity](http://femtechnet.org/csov/lock-down-your-digital-identity/)
- [Anti-doxing Guide for Activists Facing Attacks from the Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
