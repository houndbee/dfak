---
layout: page
title: "Introduction"
author: RaReNet
language: en
summary: "The Digital First Aid Kit aims to provide preliminary support for people facing the most common types of digital threats. The Kit offers a set of self-diagnostic tools for human rights defenders, bloggers, activists, journalists, and their communities facing attacks themselves, as well as providing guidelines for digital first responders to assist a person under threat."
date: 2015-08
permalink: /en/
parent: Home
---
# Welcome to the Digital First Aid Kit!

The Digital First Aid Kit is a free resource to help human rights defenders, bloggers, activists, and journalists better protect themselves and their communities against the most common types of digital threats. If you or someone you are assisting has a digital safety need, the Digital First Aid Kit will guide you in diagnosing the issues and refer you to support providers for further help if needed.

## How to use Digital First Aid Kit?

What problem are you facing?

- [I lost control of my device](topics/device-seized)
- [I lost access to my accounts](topics/account-access-issues)
- [My device is acting suspiciously](topics/device-acting-suspiciously)
- [I have received suspicious messages]()
- [My website is not working](topics/website-not-working)
- [Someone is impersonating me online]()
- [I am being harassed online](topics/harassed-online)
- [I have lost my data]()
- [Someone I know has been arrested]()

Select the problem you are facing and the Digital First Aid Kit will walk you through a set of questions to better diagnose the problem and find reseources to help you. For problems you cannot tackle on your own, the DFAK has a [list of support organizations for you to consult with](list_of_organizations).


## What else you need to know?

*The Digital First Aid Kit is not meant to serve as the ultimate solution to all your digital emergencies.* If you feel uncertain about how to address the issues you are facing, please reach out to [list_of_organizations]. If you run into any unfamiliar terms, please refer to [this glossary](https://ssd.eff.org/en/glossary/) for definitions.

## Feeling overwhelmed?

Connect directly with an organization that can provide initial support for you to navigate your needs and this resource. [Button](escalation_intake)

## Who is behind the Digital First Aid Kit?

This resource is a collaborative effort of the [Rapid Response Network](https://rarenet.org), which includes the EFF, Global Voices, Hivos & the Digital Defenders Partnership, Front Line Defenders, Internews, Freedom House, Access Now, Virtual Road, CIRCL, Open Technology Fund, as well as individual security experts who are working in the field of digital security and rapid response. It is a work in progress and we [welcome any comments, suggestions, or questions](https://gitlab.com/rarenet/dfak).
